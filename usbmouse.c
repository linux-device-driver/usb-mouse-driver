#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/slab.h>
#include <linux/usb.h>
#include <linux/bits.h>
#include <linux/mod_devicetable.h>
#include <uapi/linux/hid.h>
#include <linux/input.h>
#include <linux/usb/input.h>
#include <uapi/linux/input-event-codes.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("VIJAY M RAJ<mvr250697@gmail.com>");
MODULE_DESCRIPTION("usb mouse driver");

#ifndef DEV_NAME
#define DEV_NAME  "USB_MOUSE"
#endif

struct mouse  {
	char *data;
	char phy_path[100];
	struct urb *urb;
	struct usb_host_interface *intf;
	struct usb_endpoint_descriptor *endpoint;
	struct usb_device *device;
	struct input_dev *idev;
	dma_addr_t data_dma;
};

static struct usb_device_id usb_mouse_table[] = {
	{USB_INTERFACE_INFO(USB_INTERFACE_CLASS_HID,USB_INTERFACE_SUBCLASS_BOOT,USB_INTERFACE_PROTOCOL_MOUSE)},
	{}
};

MODULE_DEVICE_TABLE(usb,usb_mouse_table);

/* -------------------------------------- Mouse Input Operations ----------------------------------------------------- */

static int mouse_open(struct input_dev *idev)  {

	int ret;
	struct mouse *mdev=NULL;

	mdev=(struct mouse*)input_get_drvdata(idev);

	if(!mdev)  {
		printk(KERN_ERR "input_get_drvdata failed \n");
		return -EINVAL;
	}
	ret=usb_submit_urb(mdev->urb,GFP_KERNEL);
	if(ret)  {
		printk(KERN_ERR "USB urb submission failed \n");
		return -EIO;
	}

	return 0;
}

static void mouse_close(struct input_dev *idev)  {

	struct mouse *mdev=(struct mouse*)input_get_drvdata(idev);

	usb_kill_urb(mdev->urb);
}

static void mouse_data(struct urb *urb)  {

	struct mouse *mdev=(struct mouse*)urb->context;

	switch(urb->status)  {
			case 0:
				break;
			case -ECONNRESET:
			case -ENOENT:
			case -ESHUTDOWN:
				return;

			default:
				goto resubmit;

	}

	input_report_key(mdev->idev,BTN_LEFT,mdev->data[0] & 0x01);
	input_report_key(mdev->idev,BTN_RIGHT,mdev->data[0] & 0x02);
	input_report_key(mdev->idev,BTN_MIDDLE,mdev->data[0] & 0x04);

	input_report_rel(mdev->idev,REL_X,mdev->data[1]);
	input_report_rel(mdev->idev,REL_Y,mdev->data[2]);
	input_report_rel(mdev->idev,REL_WHEEL,mdev->data[3]);

	input_sync(mdev->idev);

resubmit:
		if(usb_submit_urb(urb,GFP_ATOMIC)<0)
			printk(KERN_ERR "USB urb submission failed \n");
}

/* ------------------------------------------------------------------------------------------------------------------- */

/* ------------------------------------- USB driver probe and disconnect --------------------------------------------- */

static int usb_mouse_probe(struct usb_interface *intf, const struct usb_device_id *id)  {

	int pipe,maxp,error;
	struct mouse *mdev;

	mdev=(struct mouse *)kmalloc(sizeof(struct mouse),GFP_KERNEL);
	if(!mdev)  {
		printk(KERN_ERR "kmalloc failed \n");
		return -EINVAL;
	}


	mdev->intf=intf->cur_altsetting;

	if(mdev->intf->desc.bNumEndpoints)  {
		int i;
		for(i=0;i<=mdev->intf->desc.bNumEndpoints;++i)  {
			mdev->endpoint=&mdev->intf->endpoint[i].desc;
			if(((mdev->endpoint->bmAttributes & USB_ENDPOINT_XFERTYPE_MASK) == USB_ENDPOINT_XFER_INT) && (mdev->endpoint->bEndpointAddress & USB_DIR_IN))
				break;
		}
	}

	mdev->device=interface_to_usbdev(intf);
	if(!mdev->device)  {
		printk(KERN_INFO "unable to fetch device info \n");
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
		return -EINVAL;
	}

	pipe=usb_rcvintpipe(mdev->device,mdev->endpoint->bEndpointAddress); 				/* creates interrupt rcv pipe */
	maxp=usb_maxpacket(mdev->device,pipe,usb_pipeout(pipe)); 							/* returns max packet limit of pipe */

	mdev->data=usb_alloc_coherent(mdev->device,8,GFP_ATOMIC,&mdev->data_dma); 		/* allocates dma buffer */
	if(!mdev->data)  {
		printk(KERN_ERR "usb dma buffer could not be allocated \n");
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
		return -ENOSPC;
	}

	mdev->urb=usb_alloc_urb(0,GFP_KERNEL); 								/* allocated space for USB request block */
	if(!mdev->urb)  {
		printk(KERN_ERR "unable to allocate request block \n");
		usb_free_coherent(mdev->device,sizeof(char)*8,mdev->data,mdev->data_dma);
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
		return -EINVAL;
	}

	mdev->idev=input_allocate_device();
	if(!mdev->idev)  {
		printk(KERN_ERR "unable to allocate input driver\n");
		usb_free_urb(mdev->urb);
		usb_free_coherent(mdev->device,sizeof(char)*8,mdev->data,mdev->data_dma);
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
		return -EINVAL;
	}

	mdev->idev->name="usb_mouse";
	if(usb_make_path(mdev->device,mdev->phy_path,sizeof(mdev->phy_path))<0)  {
		
		input_free_device(mdev->idev);	
		usb_free_urb(mdev->urb);
		usb_free_coherent(mdev->device,sizeof(char)*8,mdev->data,mdev->data_dma);
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
		return -EINVAL;
	}
	
	strlcat(mdev->phy_path,"/input0",sizeof(mdev->phy_path));
	mdev->idev->phys=mdev->phy_path;

	usb_to_input_id(mdev->device,&mdev->idev->id);
	mdev->idev->dev.parent=&intf->dev;

	mdev->idev->evbit[0]=BIT_MASK(EV_KEY) | BIT_MASK(EV_REL);
	mdev->idev->keybit[BIT_WORD(BTN_MOUSE)]=BIT_MASK(BTN_LEFT) | 
		BIT_MASK(BTN_RIGHT) | BIT_MASK(BTN_MIDDLE);
	mdev->idev->relbit[0]=BIT_MASK(REL_X) | BIT_MASK(REL_Y);
	mdev->idev->relbit[0] |= BIT_MASK(REL_WHEEL);

	input_set_drvdata(mdev->idev,mdev);

	mdev->idev->open=mouse_open;
	mdev->idev->close=mouse_close;

	usb_fill_int_urb(mdev->urb,mdev->device,pipe,mdev->data,(maxp > 8?8 : maxp),mouse_data,mdev,mdev->endpoint->bInterval);
	mdev->urb->transfer_dma=mdev->data_dma;
	mdev->urb->transfer_flags |= URB_NO_TRANSFER_DMA_MAP;

	error=input_register_device(mdev->idev);
	if(error)  {
		printk(KERN_ERR "input device registration failed \n");
		usb_free_urb(mdev->urb);
		usb_free_coherent(mdev->device,sizeof(char)*8,mdev->data,mdev->data_dma);
		input_free_device(mdev->idev);
		memset(mdev,'\0',sizeof(struct mouse));
		kfree(mdev);
	}

	usb_set_intfdata(intf,(void*)mdev);

	return 0;
}

void usb_mouse_disconnect(struct usb_interface *intf) {
	
	struct mouse *mdev;
	
	mdev=(struct mouse*)usb_get_intfdata(intf);

	usb_kill_urb(mdev->urb);
	input_unregister_device(mdev->idev);
	usb_free_urb(mdev->urb);
	usb_free_coherent(mdev->device,sizeof(char)*8,mdev->data,mdev->data_dma);
	memset(mdev,'\0',sizeof(struct mouse));
	kfree(mdev);
}

/* ------------------------------------------------------------------------------------------------- */

static struct usb_driver usb_mouse_driver = {
	.name=DEV_NAME,
	.probe=usb_mouse_probe,
	.disconnect=usb_mouse_disconnect,
	.id_table=usb_mouse_table
};

static int __init usb_mouse_init(void)  {

	if(usb_register(&usb_mouse_driver) == -1)  {
		printk(KERN_ERR "usb registering failed \n");
		return -EINVAL;
	}

	return 0;
}


static void __exit usb_mouse_exit(void)  {
	usb_deregister(&usb_mouse_driver);
}

module_init(usb_mouse_init);
module_exit(usb_mouse_exit);
